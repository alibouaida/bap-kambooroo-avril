<?php

return [

    'age' => [
        '0to3'      =>      '0 - 3 ans',
        '3to6'      =>      '3 - 6 ans',
        '6to12'     =>      '6 - 12 ans',
        '12to14'    =>      '12 - 14 ans',
    ],


];
