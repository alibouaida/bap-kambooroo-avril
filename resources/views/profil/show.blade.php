@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Profil</div>

                    <div class="panel-body">
                        <div>
                            <p>{{ $user->name }}</p>
                            <p>{{ $user->email }}</p>
                            <p>{{ $user->phone}}</p>
                            <p>{{ $user->city}}</p>
                            <p>{{ $user->postal}}</p>

                            @if(Auth::user()->image_name !== 'defaul.png')
                                <img src="{{ asset('images/avatars/'.$user->image_name) }}" alt="" class="img-responsive img-thumbnail" height="200" width="200">
                            @endif
                        </div>
                        <a class="btn btn-default" role="button" href="{{ route('profil.edit', ['slug' => Auth::user()->slug]) }}">
                                Modifier mes informations
                        </a>

                        @if (Auth::user()->password != [])
                            <a class="btn btn-default" role="button" href="{{ route('changePassword') }}">Changer de mot de passe</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection