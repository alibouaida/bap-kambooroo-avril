@extends('layouts.app')

@section('content')

    <div class="site-content">
        <div class="container">
            <h3 class="content-title">Fiches Conseils</h3>
            <hr class="content-divider">
            <div class="text-center">
                <p>Astuces et informations pour les parents au quotidien.</p>
            </div>

            <!-- Cards
            <div class="row">

                <div class="col-md-4">
                    <div class="card text-center">
                        <img class="card-img-top" src="https://previews.123rf.com/images/famveldman/famveldman1512/famveldman151200063/49486322-children-playing-with-wooden-train-toddler-kid-and-baby-play-with-blocks-trains-and-cars-educational.jpg" alt="Card image cap">
                        <div class="card-body">
                            <div class="card-age mx-auto d-block">
                                2 - 3
                                <br>
                                <strong>ans</strong>
                            </div>
                            <div class="card-title">Fiche conseil n°1</div>
                            <div class="card-text">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </div>
                            <div class="card-button-group">
                                <a class="card-button-outline" href="#">Télécharger</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card text-center">
                        <img class="card-img-top" src="https://www.todaysparent.com/wp-content/uploads/2017/09/unexpected-ways-outdoor-play-can-benefit-your-kid-1024x576-1499960820.jpg" alt="Card image cap">
                        <div class="card-body">
                            <div class="card-age mx-auto d-block">
                                2 - 3
                                <br>
                                <strong>ans</strong>
                            </div>
                            <div class="card-title">Fiche conseil n°1</div>
                            <div class="card-text">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </div>
                            <div class="card-button-group">
                                <a class="card-button-outline" href="#">Télécharger</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card text-center">
                        <img class="card-img-top" src="https://previews.123rf.com/images/famveldman/famveldman1512/famveldman151200063/49486322-children-playing-with-wooden-train-toddler-kid-and-baby-play-with-blocks-trains-and-cars-educational.jpg" alt="Card image cap">
                        <div class="card-body">
                            <div class="card-age mx-auto d-block">
                                2 - 3
                                <br>
                                <strong>ans</strong>
                            </div>
                            <div class="card-title">Fiche conseil n°1</div>
                            <div class="card-text">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </div>
                            <div class="card-button-group">
                                <a class="card-button-outline" href="#">Télécharger</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card text-center">
                        <img class="card-img-top" src="https://previews.123rf.com/images/famveldman/famveldman1512/famveldman151200063/49486322-children-playing-with-wooden-train-toddler-kid-and-baby-play-with-blocks-trains-and-cars-educational.jpg" alt="Card image cap">
                        <div class="card-body">
                            <div class="card-age mx-auto d-block">
                                2 - 3
                                <br>
                                <strong>ans</strong>
                            </div>
                            <div class="card-title">Fiche conseil n°1</div>
                            <div class="card-text">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </div>
                            <div class="card-button-group">
                                <a class="card-button-outline" href="#">Télécharger</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card text-center">
                        <img class="card-img-top" src="https://previews.123rf.com/images/famveldman/famveldman1512/famveldman151200063/49486322-children-playing-with-wooden-train-toddler-kid-and-baby-play-with-blocks-trains-and-cars-educational.jpg" alt="Card image cap">
                        <div class="card-body">
                            <div class="card-age mx-auto d-block">
                                2 - 3
                                <br>
                                <strong>ans</strong>
                            </div>
                            <div class="card-title">Fiche conseil n°1</div>
                            <div class="card-text">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </div>
                            <div class="card-button-group">
                                <a class="card-button-outline" href="#">Télécharger</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div> -->

            @if(count($sheets) > 0)

                @foreach($sheets as $sheet)

                    <div class="well">
                        <img class="card-img-top" src="{{ asset('images/sheets/'.$sheet->image) }}" class="img-fluid">
                        <h1><a href="{{ asset('pdf/sheets/'. $sheet->file_name) }}">{{$sheet->title}}</a></h1>
                        <span>{{$sheet->ageRange()}}</span>
                        <p>{{$sheet->content}}</p>
                        <hr>
                        <small>{{$sheet->user->name}} |{{$sheet->created_at}}</small>
                    </div>

                @endforeach

            @else

                <p>Erreur: aucune fiche conseils disponible</p>

            @endif

        </div>
    </div>


@endsection
