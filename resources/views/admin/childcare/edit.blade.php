@extends('admin.app')

@section('content')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('admin.childcare.index') }}">Kambooroo Administration</a>
        </li>
        <li class="breadcrumb-item active">Gardes</li>
    </ol>
    <h1>Édition de la garde </h1>

            @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
            <form method="post" action="{{ route('childcare.update', $id) }}" class="form-section">

                {{ csrf_field() }}
                {{ method_field('patch') }}

                <div class="form-group{{ $errors->has('children') ? ' has-error' : '' }}">
                    <label for="children" class="col-md-4 control-label">Nombre d'enfants</label>

                    <div class="col-md-6">
                        <select id="children" type="text" class="form-control" name="children" required
                                autofocus>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                        </select>

                        @if ($errors->has('children'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('children') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('begining') ? ' has-error' : '' }}">
                    <label for="begining" class="col-md-4 control-label">Début</label>

                    <div class="col-md-6">
                        <input id="begining" type="text" class="form-control" name="begining"
                               value="{{ $childcare->begining }}" required autofocus>

                        @if ($errors->has('begining'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('begining') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('end') ? ' has-error' : '' }}">
                    <label for="end" class="col-md-4 control-label">Fin</label>

                    <div class="col-md-6">
                        <input id="end" type="text" class="form-control" name="end" value="{{ $childcare->end }}"
                               required autofocus>

                        @if ($errors->has('end'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('ennd') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('age_range') ? ' has-error' : '' }}">
                    <label for="age_range" class="col-md-4 control-label">Tranche d'âge</label>

                    <div class="col-md-6">
                        <select id="age_range" type="text" class="form-control" name="age_range" required
                                autofocus>
                            <option>Nouveau né (0 - 10 mois)</option>
                            <option>Nourrisson (10 mois - 2 ans)</option>
                            <option>Petit enfant (2 ans - 6 ans)</option>

                        </select>

                        @if ($errors->has('age_range'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('age_range') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('day') ? ' has-error' : '' }}">
                    <label for="day" class="col-md-4 control-label">Jour</label>

                    <div class="col-md-6">
                        <input id="day" type="text" class="form-control" name="day" value="{{ $childcare->day }}"
                               required autofocus>

                        @if ($errors->has('day'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('day') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                    <label for="location" class="col-md-4 control-label">Localisation</label>

                    <div class="col-md-6">
                        <input id="location" type="text" class="form-control" name="location"
                               value="{{ $childcare->location }}" required autofocus>

                        @if ($errors->has('location'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea class="form-control" rows="10" cols="auto" placeholder="Prénom des enfants à garder, âge, consignes particulières..." id="description" name="description" required>{{ $childcare->description }}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Publier
                        </button>
                    </div>
                </div>

            </form>

@endsection