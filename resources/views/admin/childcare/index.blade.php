@extends('admin.app')

@section('content')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('admin.childcare.index') }}">Kambooroo Administration</a>
        </li>
        <li class="breadcrumb-item active">Gardes</li>
    </ol>
                        <div class="panel-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <hr>
                            @if(count($childcares) > 0)

                                <table class="table table-striped table-responsive">
                                    <tr>
                                        <th>Liste des gardes</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    @foreach($childcares as $childcare)
                                        <tr>
                                            <td>Garde de {{$childcare->user->name}} du {{$childcare->day}} pour {{$childcare->age_range}} sur {{$childcare->location}}  </td>
                                            <td><a class="btn btn-primary" href="{{ route('admin.childcare.edit', $childcare->id)}}">Modifier</a></td>
                                            <td>
                                                <form action="{{ route('admin.childcare.destroy', $childcare->id)}}" method="POST">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                    <button class="btn btn-danger">Supprimer</button>
                                                </form></td>
                                        </tr>
                                    @endforeach
                                </table>

                            @else
                                <p>Pas de garde pour le moment</p>
                            @endif
                        </div>
                        @endsection

