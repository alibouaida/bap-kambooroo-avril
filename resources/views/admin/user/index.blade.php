@extends('admin.app')

@section('content')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('admin.user.index') }}">Kambooroo Administration</a>
        </li>
        <li class="breadcrumb-item active">Utilisateurs</li>
    </ol>


    <!-- Content -->
    <div class="panel-body">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <hr>
        @if(count($users) > 0)

            <table class="table table-striped table-responsive">
                <tr>
                    <th>Liste des utilisateurs</th>
                    <th></th>
                    <th></th>
                </tr>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->name}}</td>
                        <td><a class="btn btn-primary" href="{{ route('admin.user.edit', $user->id)}}">Modifier</a></td>
                        <td>
                            <form action="{{ route('admin.user.destroy', $user->id)}}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <button class="btn btn-danger">Supprimer</button>
                            </form></td>
                    </tr>
                @endforeach
            </table>

        @else
            <p>Pas d'article pour le moment</p>
        @endif
    </div>
@endsection

