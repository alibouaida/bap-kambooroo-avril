@extends('admin.app')

@section('content')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('admin.sheet.index') }}">Kambooroo Administration</a>
        </li>
        <li class="breadcrumb-item active">Fiches conseils</li>
    </ol>


    <!-- Content -->
    <div class="panel-body">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <a href="{{ route('admin.sheet.create')}}" class="btn btn-primary">Ajouter une fiche</a>
        <hr>
        @if(count($sheets) > 0)

            <table class="table table-striped table-responsive">
                <tr>
                    <th>Liste des fiches conseils</th>
                    <th></th>
                    <th></th>
                </tr>
                @foreach($sheets as $sheet)
                    <tr>
                        <td>{{$sheet->title}}</td>
                        <td><a class="btn btn-primary" href="{{ route('admin.sheet.edit', $sheet->id)}}">Modifier</a></td>
                        <td>
                            <form action="{{ route('admin.sheet.destroy', $sheet->id)}}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <button class="btn btn-danger">Supprimer</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>

        @else
            <p>Pas de fiche pour le moment</p>
        @endif
    </div>
@endsection

