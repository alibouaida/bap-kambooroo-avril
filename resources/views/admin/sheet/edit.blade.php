@extends('admin.app')

@section('content')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('admin.sheet.index') }}">Kambooroo Administration</a>
        </li>
        <li class="breadcrumb-item active">Fiches conseils</li>
    </ol>

    <h1>Création de fiche conseil</h1>



    <div class="panel-body">

        @if(session('message'))
            <div class='alert alert-success'>
                {{ session('message') }}
            </div>
        @endif

        <form class="form-horizontal" enctype="multipart/form-data" method="POST" action="{{ route('admin.sheet.update' ,$sheet->id) }}">
            {{ csrf_field() }}
            {{ method_field('patch') }}

            <div class="form-group">
                <label name="title">Titre :</label>
                <input id="title" type="text" name="title" class="form-control" value="{{$sheet->title }}">
            </div>
            <div class="form-group">
                <label name="content">Age :</label>
                <select name="age_range" >
                    <option value="{{ $sheet->age_range }}">0-3 ans</option>
                    <option value="{{ $sheet->age_range }}">3-6 ans</option>
                    <option value="{{ $sheet->age_range }}">6-12 ans</option>
                    <option value="{{ $sheet->age_range }}">12-14 ans</option>
                </select>
            </div>
            <div class="form-group">
                <label name="content">Contenu :</label>
                <input id="content" type="text" name="content" class="form-control" value="{{ $sheet->content}}">
            </div>
            <div class="form-group">
                <label for="image" class="control-label">Photo de la fiche</label>
                <input type="file" class="form-control" name="image"  id="image" value="{{ $sheet->image}}"/>
            </div>

            <div class="form-group">
                <label for="image" class="control-label">PDF de la fiche</label>
                <input type="file" class="form-control" name="file_name"  id="file_name" value="{{ $sheet->file_name}}"/>
            </div>
            <input type="submit" value="Modifier la fiche" class="btn btn-success">
        </form>
    </div>


@endsection
