@extends('layouts.app')

@section('content')

    <!-- Content Section -->
    <div class="site-content">
        <div class="welcome">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <h2 class="welcome-title">Bienvenue sur votre plateforme Kambooroo !</h2>
            <p class="d-none">Avant de vous lancer dans l'aventure, prenez le temps de d'apprendre à utiliser notre service.</p>
            <!-- <a class="btn btn-primary mt-3" href="#">En Savoir Plus</a> -->
        </div>
    </div>

@endsection
