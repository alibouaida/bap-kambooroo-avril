<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Kambooroo') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Poppins" rel="stylesheet">

    <link rel="icon" type="image/png" href="{{ asset('images/favicon/favicon-32x32.png?v=2') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('images/favicon/site.webmanifest') }}">
    <link rel="mask-icon" href="{{ asset('images/favicon/safari-pinned-tab.svg') }}" color="#B24592">
    <meta name="msapplication-TileColor" content="#B24592">
    <meta name="theme-color" content="#B24592">

    <style>
        .site-content {
            min-height: 100vh;
        }

        .content-button-active {
            color: #fff !important;
        }

        .box-info {
            flex-direction: column;
            display: flex;
            align-items: center;
            margin-bottom: 30px;
            margin-top: 30px;
        }

        .box-info span {
            color: #B24592 !important;
            font-size: 1.2em;
        }

        body {
            background-color: #f1f1f1;
        }

        a {
            color: #B24592 !important;
        }

        .alert-update-profile {
            width: 50%;
            text-align: center;
            margin-left: 300px
        }

        @media screen and (max-width: 700px) {
            .alert-update-profile {
                margin-left: 70px;
                width: 80%;
            }
        }

        .btn-social {
            position: relative;
            padding-left: 44px;
            text-align: left;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .btn-facebook {
            color: #fff !important;
            background-color: #3b5998;
            border-color: rgba(0, 0, 0, 0.2);
        }

        .btn-google {
            color: #fff !important;
            background-color: #dd4b39;
            border-color: rgba(0, 0, 0, 0.2);
        }

    </style>
</head>
<body>


@section('sidebar')
    @auth

        <div class="site-sidebar">
            <div class="sidebar-header">
                <a class="" href="{{ route('home') }}"><img class="sidebar-logo" src="{{ asset('images/logo.svg') }}" alt="" width=""></a>
                <a href="{{ route('home') }}"><img class="sidebar-logo-single" src="{{ asset('images/logo_single.svg') }}" alt=""></a>
            </div>

            <nav>
                <ul class="sidebar-list">
                    <li class="sidebar-item">
                        <a class="sidebar-icon" href="{{ route('home') }}"><i class="fas fa-home fa-1x"></i></a>
                        <a class="sidebar-button" href="{{ route('home') }}">Accueil</a>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-icon" href="{{ route('network.index') }}"><i class="fas fa-users fa-1x"></i></a>
                        <a class="sidebar-button" href="{{ route('network.index') }}">Réseau</a>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-icon" href="{{ route('childcare.index') }}"><i class="fas fa-shield-alt fa-1x"></i></a>
                        <a class="sidebar-button" href="{{ route('childcare.index') }}">Garde</a>
                    </li>
                <!--
                    <li class="sidebar-item">
                        <a class="sidebar-icon" href="{{ route('workshop.index') }}"><i class="fas fa-puzzle-piece fa-1x"></i></a>
                        <a class="sidebar-button" href="{{ route('workshop.index') }}">Ateliers</a>
                    </li>
                    -->
                    <li class="sidebar-item">
                        <a class="sidebar-icon" href="{{ route('sheet.index') }}"><i class="fas fa-align-justify fa-1x"></i></a>
                        <a class="sidebar-button" href="{{ route('sheet.index') }}">Conseils</a>
                    </li>
                </ul>
                <hr class="sidebar-divider-nav">
                <ul class="sidebar-list">
                    <li class="sidebar-item">
                        <a class="sidebar-icon" href="{{ route('user.edit', ['slug' => Auth::user()->slug]) }}"><i class="fas fa-user fa-1x"></i></a>
                        <a class="sidebar-button" href="{{ route('user.edit', ['slug' => Auth::user()->slug]) }}">Profil</a>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-icon" href="{{ route('contact') }}"><i class="fas fa-envelope fa-1x"></i></a>
                        <a class="sidebar-button" href="{{ route('contact') }}">Contact</a>
                    </li>
                    @if(Auth::user()->isAdmin())
                        <li class="sidebar-item">
                            <a class="sidebar-icon" href="{{ route('admin') }}"><i class="fas fa-columns fa-1x"></i></a>
                            <a class="sidebar-button" href="{{ route('admin') }}">Administration</a>
                        </li>
                    @endif
                    <li class="sidebar-item">
                        <a class="sidebar-icon" href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt fa-1x"></i></a>
                        <a class="sidebar-button" href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">Déconnexion</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </nav>
        </div>




        <!-- Logout Modal -->
        <div class="modal fade" id="LogoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content text-center">
                    <div class="modal-body">
                        <p class="modal-info">Etes vous sûre de vouloir vous deconnectez ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Non</button>
                        <button type="button" class="btn btn-primary">Oui</button>
                    </div>
                </div>
            </div>
        </div>

        <a href="{{ route('notifications.index') }}" style="position: absolute;right: 0;top: 50px;z-index: 89878;">
            @if(count(Auth::user()->unReadNotifications))
                <span class="badge badge-pill badge-light" style="color: #B24592;font-size: 1.2em;background-color: #ffffff;border: 1px solid #B24592;position: absolute;    right: 30px;
    top: -20px;">
                        {{ count(Auth::user()->unReadNotifications) }}
                    </span>
            @else
            @endif

        </a>
        <a href="{{ route('notifications.index') }}">
            <i class="fas fa-bell fa-2x content-notification" style="    position: absolute;
    right: 0;"></i>

        </a>

    @endauth
@show


@auth
    @if(Auth::user()->city == [] && Auth::user()->postal == [] && Auth::user()->phone == [])
        <div class="container d-flex justify-content-center">
            <div class="alert alert-danger alert-update-profile" role="alert">
                <a href="{{ url('user/'.Auth::user()->slug.'/edit') }}"><i class="fas fa-exclamation-triangle"></i> Vous devez finaliser votre profil</a>
            </div>
        </div>
    @endif
@endauth


@yield('content')



<!-- Scripts -->
<script src="{{ asset('js/main.js') }}"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

</body>
</html>
