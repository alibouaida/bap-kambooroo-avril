@extends('layouts.web')

@section('navbar')
    <?php $navbar = "dark"; ?>

    <!-- Header Section -->
    <header class="header-gradient">

        <nav class="navbar navbar-expand-lg navbar-dark py-4 text-center">
            @parent
        </nav>

        @section('logo', asset('images/logo.svg'))

        @endsection

        @section('content')


            <section class="feature-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 mb-5">
                            <h2 class="text-light">Kambooroo, la plateforme de garde d’enfants dédiée aux parents</h2>
                            <p class="text-light">Kambooroo est une plateforme collaborative 100% gratuite créée par des parents pour des parents !</p>
                        </div>
                        <div class="col-md-6">
                            <img src="{{ asset('images/illustration_1.svg') }}" alt="">
                        </div>
                    </div>
                </div>
            </section>
    </header>
    <section class="services-content text-center my-5">
        <div class="container">
            <div class="card p-5">
                <div class="row">
                    <div class="col-md-4">
                        <i class="fas fa-puzzle-piece fa-5x my-4"></i>
                        <h3>Ateliers</h3>
                        <p class="my-3">
                            Profitez d'ateliers pédagogiques pour recevoir des conseils de professionnels.
                        </p>
                    </div>
                    <div class="col-md-4">
                        <i class="fas fa-users fa-5x my-4"></i>
                        <h3>Créez un réseau</h3>
                        <p class="my-3">
                            Créez votre propre réseau de confiance et partagez la garde de vos enfants entre jeunes parents.
                        </p>
                    </div>
                    <div class="col-md-4">
                        <i class="fas fa-shield-alt fa-5x my-4"></i>
                        <h3>Partage de garde</h3>
                        <p class="my-3">
                            Autorisez-vous des moments de détente grâce à une meilleure organisation au quotidien.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="subscribe-content">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-center">
                    <img class="img-fluid" src="{{ asset('images/illustration_2.png') }}" alt="">
                </div>
                <div class="col-md-6 mt-5">
                    <h3 class="text-light">Inscrivez vous gratuitement dès maintenant</h3>
                    <p class="text-light">Rejoignez des milliers de familles pour apprendre à mieux connaître vos enfants.</p>
                    <a href="{{ route('register') }}" class="btn nav-btn-lg">S'inscrire</a>
                </div>
            </div>
        </div>
    </section>

@endsection

