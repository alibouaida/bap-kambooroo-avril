@extends('layouts.app')

@section('content')


    <div class="site-content">
        <div class="container">
            <h3 class="content-title">Gardes</h3>
            <hr class="content-divider">

            <div class="content-button-group mt-5">
                <a class="content-button-active" href="{{ route('childcare.myneeds') }}">Mes besoins</a>
                <a class="content-button" href="{{ route('childcare.index') }}">Besoin du réseau</a>
                <a class="content-button" href="{{ route('childcare.mychildcares') }}">Gardes en attentes / acceptées</a>
            </div>
            @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
            @if (session('childcare_delete'))
                <div class="alert alert-warning">
                    {{ session('childcare_delete') }}
                </div>
            @endif
            @if (session('childcare_refused'))
                <div class="alert alert-warning col-12">
                    {{ session('childcare_refused') }}
                </div>
            @endif
            <div class="card offset-md-1 col-md-10">
                <div class="card-body">
                    <h3 class="content-title">J'ai besoin d'une garde</h3>
                    <hr class="content-divider">
                    <form method="post" action="{{ route('childcare.store') }}" class="form-section">

                        {{ csrf_field() }}

                        <div class="form-row">
                            <div class="form-group col-md-6{{ $errors->has('children') ? ' has-error' : '' }}">
                                <label for="children" class="control-label">Nombre d'enfants</label>
                                <select id="children" type="text" class="form-control" name="children" required
                                        autofocus>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                </select>

                                @if ($errors->has('children'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('children') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6 {{ $errors->has('age_range') ? ' has-error' : '' }}">
                                <label for="age_range" class="control-label">Tranche d'âge</label>
                                <select id="age_range" type="text" class="form-control" name="age_range"
                                        required
                                        autofocus>
                                    <option>0 - 3 ans</option>
                                    <option>3 - 6 ans</option>
                                    <option>6 - 12 ans</option>
                                    <option>12 - 14 ans</option>

                                </select>

                                @if ($errors->has('age_range'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('age_range') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6 {{ $errors->has('begining') ? ' has-error' : '' }}">
                                <label for="begining" class="control-label">Début</label>

                                <input id="begining" type="time" class="form-control" name="begining"
                                       value="{{ old('begining') }}" required autofocus>

                                @if ($errors->has('begining'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('begining') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6 {{ $errors->has('end') ? ' has-error' : '' }}">
                                <label for="end" class="control-label">Fin</label>
                                <input id="end" type="time" class="form-control" name="end"
                                       value="{{ old('end') }}"
                                       required autofocus>

                                @if ($errors->has('end'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('end') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="form-group col-md-6 {{ $errors->has('day') ? ' has-error' : '' }}">
                                <label for="day" class="control-label">Jour</label>
                                <input id="day" type="date" placeholder="MM/DD/YYY" class="form-control"
                                       name="day"
                                       value="{{ old('day') }}"
                                       required autofocus>

                                @if ($errors->has('day'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('day') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6 {{ $errors->has('location') ? ' has-error' : '' }}">
                                <label for="location" class="control-label">Localisation</label>

                                <input id="location" placeholder="Choisir une localisation" type="text"
                                       class="form-control"
                                       name="location"
                                       value="{{ old('location') }}" required autofocus>

                                @if ($errors->has('location'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea class="form-control" rows="10" cols="auto" placeholder="Prénom des enfants à garder, âge, consignes particulières..." id="description" name="description" required>{{ old('description') }}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="submit-form-button">
                            <button type="submit" class="btn btn-primary">Publier</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <div class="row justify-content-center col-12">
            <h3 class="welcome-title">
                Ou
            </h3>
        </div>
        @if(count($myneeds) === 0)
            <div class="card offset-md-1 col-md-10">
                <div class="card-body">
                    <h3 class="content-title">Mes besoins</h3>
                    <hr class="content-divider">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <p class="text-center">Aucune demande en cours ...</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @else
            <div class="card offset-md-1 col-md-10">
                <div class="card-body">
                    <h3 class="content-title">Mes besoins</h3>
                    <hr class="content-divider">
                    <div class="row">
                        @foreach($myneeds as $ad)
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="row col-md-8">
                                                <div>
                                                    <img class="rounded-circle" src="{{ $ad->user->avatar() }}" alt=""
                                                         width="120" height="120">
                                                </div>
                                                <div class="col-md-6" >
                                                    <div class="card-title">{{ $ad->user->name }}</div>
                                                    @if($ad->accepted_by != null)
                                                        <div class="card-subtitle">Demande de garde acceptée par
                                                            <strong>{{ $ad->accepted_by()->name }}</strong></div>
                                                    @else
                                                        <div class="card-subtitle">Personne ne s'est encore proposé pour
                                                            cette garde
                                                        </div>
                                                    @endif
                                                    <div class="card-text">
                                                        <p>
                                                            Adresse: <br>
                                                            {{ $ad->location }}
                                                        </p>
                                                        <p>
                                                            Nombre d'enfants: <br>
                                                            {{ $ad->children }}
                                                        </p>
                                                        <p>
                                                            Description: <br>
                                                            {{ $ad->description }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row col-md-4">
                                                <div class="col-md-12 row justify-content-end">
                                                    <div>{{ $ad->day }} <br> {{ $ad->begining }} à {{ $ad->end }}</div>
                                                </div>
                                                <div class="col-md-12 align-self-end">
                                                    <div class="card-button-group">
                                                        @if($ad->accepted_by != null && $ad->accepted === 0)
                                                            <a class="card-button-outline"
                                                               href="{{ route('childcare.refused', ['id' => $ad->id]) }}">Refuser</a>
                                                            <a class="card-button-outline"
                                                               href="{{ route('childcare.confirm', ['id' => $ad->id]) }}">Accepter</a>
                                                            <a class="card-button-outline"
                                                               href="{{ route('childcare.edit', ['id' => $ad->id]) }}">Modifier</a>
                                                        @elseif($ad->accepted === 1)
                                                            <a class="card-button-outline"
                                                               href="{{ route('childcare.refused', ['id' => $ad->id]) }}">Refuser</a>
                                                            <a class="card-button-outline"
                                                               href="{{ route('childcare.delete', ['id' => $ad->id]) }}">Annuler</a>
                                                            <a class="card-button-outline"
                                                               href="{{ route('childcare.edit', ['id' => $ad->id]) }}">Modifier</a>
                                                        @else
                                                            <a class="card-button-outline"
                                                               href="{{ route('childcare.edit', ['id' => $ad->id]) }}">Modifier</a>
                                                            <a class="card-button-outline"
                                                               href="{{ route('childcare.delete', ['id' => $ad->id]) }}">Annuler</a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
    </div>


@endsection