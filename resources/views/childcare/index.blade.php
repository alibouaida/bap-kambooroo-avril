@extends('layouts.app')

@section('content')

    <div class="site-content">
        <div class="container">
            <h3 class="content-title">Gardes</h3>
            <hr class="content-divider">

            <div class="content-button-group mt-5">
                <a class="content-button" href="{{ route('childcare.myneeds') }}">Mes besoins</a>
                <a class="content-button-active" href="{{ route('childcare.index') }}">Besoin du réseau</a>
                <a class="content-button" href="{{ route('childcare.mychildcares') }}">Gardes en attentes / acceptées</a>
            </div>
            <div class="row">
                @if (session('garde_accept'))
                    <div class="alert alert-success col-12">
                        {{ session('garde_accept') }} <a href="{{ route('childcare.mychildcares') }}">Gardes en attentes / acceptées</a>
                    </div>
                @endif
                    @if(count($childcares) === 0)
                        <div class="card offset-md-1 col-md-10">
                            <div class="card-body">
                                <h3 class="content-title">Besoins du réseau</h3>
                                <hr class="content-divider">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <p class="text-center">Aucune demande en cours ...</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @else
                    <div class="card offset-md-1 col-md-10">
                        <div class="card-body">
                            <h3 class="content-title">Mes besoins</h3>
                            <hr class="content-divider">
                            <div class="row">
                                @foreach($childcares as $ad)
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="row col-md-8">
                                                        <div>
                                                            <img class="rounded-circle" src="{{ $ad->user->avatar() }}"
                                                                 alt=""
                                                                 width="120" height="120">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="card-title">{{ $ad->user->name }}</div>
                                                            @if($ad->accepted_by != null)
                                                                <div class="card-subtitle">Demande de garde acceptée par
                                                                    <strong>{{ $ad->accepted_by()->name }}</strong>
                                                                </div>
                                                            @else
                                                                <div class="card-subtitle">Personne ne s'est encore
                                                                    proposé pour
                                                                    cette garde
                                                                </div>
                                                            @endif
                                                            <div class="card-text">
                                                                <p>
                                                                    Adresse: <br>
                                                                    {{ $ad->location }}
                                                                </p>
                                                                <p>
                                                                    Nombre d'enfants: <br>
                                                                    {{ $ad->children }}
                                                                </p>
                                                                <p>
                                                                    Description: <br>
                                                                    {{ $ad->description }}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row col-md-4">
                                                        <div class="col-md-12 row justify-content-end">
                                                            <div>{{ $ad->day }}<br> {{ $ad->begining }}
                                                                à {{ $ad->end }}</div>
                                                        </div>
                                                        <div class="col-md-12 align-self-end">
                                                            <div class="card-button-group">
                                                                @if($ad->user_id != Auth::user()->id)
                                                                    <a href="{{ route('childcare.accept', ['id'=> $ad->id]) }}"
                                                                       class="card-button-outline">Accepter</a>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>





@endsection