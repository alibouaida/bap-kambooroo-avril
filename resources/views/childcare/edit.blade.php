@extends('layouts.app')

@section('content')

    <div class="site-content">
        <div class="container">
            <h3 class="content-title">Gardes</h3>
            <hr class="content-divider">

            <div class="content-button-group mt-5">
                <a class="content-button-active" href="{{ route('childcare.myneeds') }}">Mes besoins</a>
                <a class="content-button" href="{{ route('childcare.index') }}">Besoin du réseau</a>
                <a class="content-button" href="{{ route('childcare.mychildcares') }}">Gardes en attentes / acceptés</a>
            </div>

            <div class="content-subtitle">
                Mes besoins
            </div>
            @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
            <div class="row">
                <div class="card offset-md-1 col-md-10">
                    <div class="card-body">
                        <h3 class="content-title">Modification de la garde</h3>
                        <hr class="content-divider">
                        @if(Auth::user()->admin === null)
                            <form method="post" action="{{ route('childcare.update', $id) }}" class="form-section">

                                {{ csrf_field() }}
                                {{ method_field('patch') }}

                                <div class="form-row">
                                    <div class="form-group col-md-6{{ $errors->has('children') ? ' has-error' : '' }}">
                                        <label for="children" class="control-label">Nombre d'enfants</label>
                                        <select id="children" type="text" class="form-control" name="children" required
                                                autofocus>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>

                                        @if ($errors->has('children'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('children') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-md-6 {{ $errors->has('age_range') ? ' has-error' : '' }}">
                                        <label for="age_range" class="control-label">Tranche d'âge</label>
                                        <select id="age_range" type="text" class="form-control" name="age_range"
                                                required
                                                autofocus>
                                            <option>0 - 3 ans</option>
                                            <option>3 - 6 ans</option>
                                            <option>6 - 12 ans</option>
                                            <option>12 - 14 ans</option>

                                        </select>

                                        @if ($errors->has('age_range'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('age_range') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-md-6 {{ $errors->has('begining') ? ' has-error' : '' }}">
                                        <label for="begining" class="control-label">Début</label>

                                        <input id="begining" type="time" class="form-control" name="begining"
                                               value="{{ $childcare->begining }}" required autofocus>

                                        @if ($errors->has('begining'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('begining') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-md-6 {{ $errors->has('end') ? ' has-error' : '' }}">
                                        <label for="end" class="control-label">Fin</label>
                                        <input id="end" type="time" class="form-control" name="end"
                                               value="{{ $childcare->end  }}"
                                               required autofocus>

                                        @if ($errors->has('end'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('end') }}</strong>
                                    </span>
                                        @endif
                                    </div>


                                    <div class="form-group col-md-6 {{ $errors->has('day') ? ' has-error' : '' }}">
                                        <label for="day" class="control-label">Jour</label>
                                        <input id="day" type="date" placeholder="MM/DD/YYY" class="form-control"
                                               name="day"
                                               value="{{ $childcare->day }}"
                                               required autofocus>

                                        @if ($errors->has('day'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('day') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-md-6 {{ $errors->has('location') ? ' has-error' : '' }}">
                                        <label for="location" class="control-label">Localisation</label>

                                        <input id="location" placeholder="Choisir une localisation" type="text"
                                               class="form-control"
                                               name="location"
                                               value="{{ $childcare->location }}" required autofocus>

                                        @if ($errors->has('location'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Description</label>
                                        <textarea class="form-control" rows="10" cols="auto" placeholder="Prénom des enfants à garder, âge, consignes particulières..." id="description" name="description" required>{{ $childcare->description }}</textarea>
                                    </div>
                                </div>

                                <div class="submit-form-button">
                                    <button type="submit" class="btn btn-primary">Modifier la garde</button>
                                </div>

                            </form>
                        @else
                            <form method="post" action="{{ route('admin.childcare.update', $id) }}"
                                  class="form-section">

                                {{ csrf_field() }}
                                {{ method_field('patch') }}

                                <div class="form-row">
                                    <div class="form-group col-md-6{{ $errors->has('children') ? ' has-error' : '' }}">
                                        <label for="children" class="control-label">Nombre d'enfants</label>
                                        <select id="children" type="text" class="form-control" name="children"
                                                required
                                                autofocus>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>

                                        @if ($errors->has('children'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('children') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-md-6 {{ $errors->has('age_range') ? ' has-error' : '' }}">
                                        <label for="age_range" class="control-label">Tranche d'âge</label>
                                        <select id="age_range" type="text" class="form-control" name="age_range"
                                                required
                                                autofocus>
                                            <option>0 - 3 ans</option>
                                            <option>3 - 6 ans</option>
                                            <option>6 - 12 ans</option>
                                            <option>12 - 14 ans</option>

                                        </select>

                                        @if ($errors->has('age_range'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('age_range') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-md-6 {{ $errors->has('begining') ? ' has-error' : '' }}">
                                        <label for="begining" class="control-label">Début</label>

                                        <input id="begining" type="time" class="form-control" name="begining"
                                               value="{{ $childcare->begining }}" required autofocus>

                                        @if ($errors->has('begining'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('begining') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-md-6 {{ $errors->has('end') ? ' has-error' : '' }}">
                                        <label for="end" class="control-label">Fin</label>
                                        <input id="end" type="time" class="form-control" name="end"
                                               value="{{ $childcare->end  }}"
                                               required autofocus>

                                        @if ($errors->has('end'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('end') }}</strong>
                                    </span>
                                        @endif
                                    </div>


                                    <div class="form-group col-md-6 {{ $errors->has('day') ? ' has-error' : '' }}">
                                        <label for="day" class="control-label">Jour</label>
                                        <input id="day" type="date" placeholder="MM/DD/YYY" class="form-control"
                                               name="day"
                                               value="{{ $childcare->day }}"
                                               required autofocus>

                                        @if ($errors->has('day'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('day') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-md-6 {{ $errors->has('location') ? ' has-error' : '' }}">
                                        <label for="location" class="control-label">Localisation</label>

                                        <input id="location" placeholder="Choisir une localisation" type="text"
                                               class="form-control"
                                               name="location"
                                               value="{{ $childcare->location }}" required autofocus>

                                        @if ($errors->has('location'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Description</label>
                                        <textarea class="form-control" rows="10" cols="auto" placeholder="Prénom des enfants à garder, âge, consignes particulières..." id="description" name="description" required>{{ $childcare->description }}</textarea>
                                    </div>
                                </div>

                                <div class="submit-form-button">
                                    <button type="submit" class="btn btn-primary">Modifier la garde</button>
                                </div>

                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>

@endsection