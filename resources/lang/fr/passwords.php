<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Votre mot de passe a été réinitilisé!',
    'sent' => 'Nous vous avons envoyé un email avec un lien pour reinitialiser votre mot de passe!',
    'token' => 'This password reset token is invalid.',
    'user' => "Nous n'avons pas trouvé d'utilisateur correspondant à cette addresse email.",
];
