<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* REGISTER/LOGIN */
Auth::routes();
Route::get('{provider}', 'AuthSocialController@redirect')->where('provider', '(facebook|google)');
Route::get('{provider}/callback', 'AuthSocialController@callback')->where('provider', '(facebook|google)');
Route::get('/changePassword', 'HomeController@showChangePasswordForm');
Route::post('/changePassword', 'HomeController@changePassword')->name('changePassword');
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser')->name('verify.user');
// Route::get('/verify/resend/', 'Auth\LoginController@verifyAccountReSendEmail')->name('verify.user.resend');

/* DASHBOARD */
Route::get('/home', 'HomeController@index')->name('home');

/* CONTACT */
Route::get('/contact', 'ContactController@show')->name('contact');
Route::post('/contact/send', 'ContactController@send')->name('contact.send');

/* USER */
Route::resource('/user', 'UserController');

/* NETWORK */
Route::get('/network/{slug}/Addfriend', 'NetworkController@getAdd')->name('addFriend');
Route::get('/network/{slug}/Acceptfriend', 'NetworkController@getAccept')->name('Acceptfriend');
Route::get('/network/search', 'NetworkController@searchcontact')->name('network.search');
Route::get('/network/{slug}/invitations', 'NetworkController@invitations');
Route::get('network/{slug}/delete', ['uses' => 'NetworkController@destroy', 'as' => 'network.delete']);
Route::post('network/search/result', 'NetworkController@search')->name('network.search.result');
Route::resource('/network', 'NetworkController');

/* BLOG */
Route::resource('/blog', 'BlogController');

/* WORKSHOP */
Route::resource('/workshop', 'WorkshopController');

/* ADVICE SHEET */
Route::resource('/sheet', 'AdviceSheetController');
Route::get('/sheet/pdf/{id}', 'AdviceSheetController@pdf')->name('sheet.pdf');

/* NOTIFICATIONS */
Route::resource('/notifications', 'NotificationsController');

/* CHILDCARE */
Route::get('/childcare/acceptchildcare/{id}', 'ChildcareController@acceptChildcare')->name('childcare.accept');
Route::get('/childcare/confirmchildcare/{id}', 'ChildcareController@confirmChildcare')->name('childcare.confirm');
Route::get('/childcare/refusechildcare/{id}', 'ChildcareController@refuseChildcare')->name('childcare.refuse');
Route::get('/childcare/refuse/{id}', 'ChildcareController@refuse')->name('childcare.refused');
Route::get('childcare/{id}/delete', ['uses' => 'ChildcareController@destroy', 'as' => 'childcare.delete']);
Route::get('/childcare/networkneeds', 'ChildcareController@networkNeeds')->name('childcare.networkneeds');
Route::get('/childcare/myneeds', 'ChildcareController@myNeeds')->name('childcare.myneeds');
Route::get('/childcare/mychildcares', 'ChildcareController@myChildcares')->name('childcare.mychildcares');
Route::resource('/childcare', 'ChildcareController');

/* LEGAL */
Route::get('/cgu', 'LegalController@cgu')->name('legal.cgu');
Route::get('/cgu/validate', 'LegalController@validation')->name('legal.validate');
Route::get('/cgu/validate/accepted', 'LegalController@acceptCGU')->name('accept_cgu');

/* ADMIN */
Route::get('/kbr-admin', 'Admin\AdminController@index')->name('admin');
Route::name('admin.')->group(function () {
    Route::resource('/kbr-admin/blog', 'Admin\AdminBlogController');
    Route::resource('/kbr-admin/childcare', 'Admin\AdminChildCareController');
    Route::resource('/kbr-admin/sheet', 'Admin\AdminSheetController');
    Route::resource('/kbr-admin/user', 'Admin\AdminUserController');
});
Route::get('/kbr-admin/sheet/create', 'Admin\AdminSheetController@create')->name('admin.sheet.create');

// Route::get('/kbr-admin/blog/articles', 'Admin\AdminBlogController@articles')->name('admin.blog.articles');
Route::resource('/kbr-admin/childcare', 'Admin\AdminChildCareController', ['names' => 'admin.childcare']);
Route::resource('/kbr-admin/network', 'Admin\AdminNetworkController', ['names' => 'admin.network']);
Route::resource('/kbr-admin/user', 'Admin\AdminUserController', ['names' => 'admin.user']);

