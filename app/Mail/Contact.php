<?php

namespace App\Mail;

use App\Http\Requests\ContactFormRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var ContactFormRequest
     */
    protected $contactFormRequest;

    /**
     * Create a new message instance.
     *
     * @param ContactFormRequest $contactFormRequest
     */
    public function __construct(ContactFormRequest $contactFormRequest)
    {
        $this->email = $contactFormRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.contact')->with([
            'firstname' => $this->email->firstname,
            'lastname'  => $this->email->lastname,
            'email'     => $this->email->email,
            'subject'   => $this->email->subject,
            'content'   => $this->email->message,
        ])->replyTo($this->email->email)->subject($this->email->subject.' - Kambooroo.com');
    }
}
