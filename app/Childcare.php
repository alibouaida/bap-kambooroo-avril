<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Childcare extends Model
{

    public $table = 'create_childcare';


    /* User qui poste l'annonce */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function accepted_by()
    {
        $user = User::where('id', $this->accepted_by)->first();
        return $user;
    }

}
