<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class profilUncomplet
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->city == [] && Auth::user()->postal == [] && Auth::user()->phone == []){
            return redirect('/home');
        }
        return $next($request);
    }
}
