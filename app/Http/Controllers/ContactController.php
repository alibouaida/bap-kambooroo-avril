<?php

Namespace App\Http\Controllers;

use App\Mail\Contact;
use App\Notifications\InboxMessage;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactFormRequest;
use App\Admin;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('CguTrue');
        $this->middleware('profilUncomplet');
    }

    public function show()
    {
        return view('contact');
    }

    public function send(ContactFormRequest $contactFormRequest)
    {
        Mail::to('kambooroo@gmail.com')->send(new Contact($contactFormRequest));
        return redirect()->back()->with('message', 'Message envoyé avec succès !');
    }
}
