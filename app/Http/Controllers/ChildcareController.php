<?php

namespace App\Http\Controllers;

use App\Childcare;
use App\Http\Requests\StoreChildcare;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class ChildcareController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('CguTrue');
        $this->middleware('profilUncomplet');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $friends = Auth::user()->friends();
        $auth_id = Auth::user()->id;

        if (count($friends) === 0) {
            $childcares = Childcare::where('user_id', '=', $auth_id)->get();
            return view('childcare.index', compact('childcares'));
        }

        $childcares = Childcare::whereIn('user_id', Auth::user()
            ->getFriendsIds())
            ->where('accepted_by', '=', null)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('childcare.index', compact('childcares', 'date'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $id = Auth::user()->id;

        $childcares = User::find($id)->childcares;


        return view('childcare.create', compact('childcares'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreChildcare $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreChildcare $request)
    {
        $childcare = new Childcare;
        $childcare->user_id = $request->user()->id;
        $childcare->children = $request->get('children');
        $childcare->begining = $request->get('begining');
        $childcare->end = $request->get('end');
        $childcare->day = $request->get('day');
        $childcare->location = $request->get('location');
        $childcare->age_range = $request->get('age_range');
        $childcare->description = $request->get('description');

        $childcare->save();

        return back()->with('message', 'Votre annonce a bien été mise en ligne');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $childcare = DB::table('create_childcare')->find($id);

        return view('childcare.edit', compact('id', 'childcare'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $childcare = Childcare::find($id);

        $childcare->children = $request->get('children');
        $childcare->begining = $request->get('begining');
        $childcare->end = $request->get('end');
        $childcare->day = $request->get('day');
        $childcare->location = $request->get('location');
        $childcare->age_range = $request->get('age_range');
        $childcare->description = $request->get('description');

        $childcare->save();

        $myneeds = Auth::user()->childcares;


        return Redirect::route('childcare.myneeds', compact('myneeds'))->with('message', "La garde a été modifiée avec succès");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('create_childcare')
            ->delete($id);

        return back()->with('childcare_delete', 'Vous venez de supprimer une garde');
    }


    public function myNeeds()
    {
        $myneeds = Childcare::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();
        return view('childcare.myneeds', compact('myneeds', 'date'));
    }

    public function myChildcares()
    {
        $mychildcares = Childcare::where('accepted_by', Auth::user()->id)->orderBy('created_at', 'desc')->get();

        return view('childcare.mychildcares', compact('mychildcares'));
    }

    /* Permet a un user de se proposer pour une garde */
    public function acceptChildcare($id)
    {
        Auth::user()->acceptChildcare($id);

        $message = "Garde en attente d'être validée, retrouvée là dans la partie";
        
        return back()->with('garde_accept', $message);
    }

    /* Valide le user qui se propose pour la garde */
    public function confirmChildcare($id)
    {
        Auth::user()->confirmChildcare($id);

        $childcare = Childcare::find($id);
        $accepted_by = $childcare->accepted_by()->name;

        $accepted = "Vous avez bien accepté $accepted_by pour la garde du $childcare->day";

        return back()->with('message', $accepted);
    }

    /* Refuse le user qui se propose pour la garde */
    public function refuseChildcare($id)
    {
        Auth::user()->refuseChildcare($id);

        $message = "Vous venez de refuser une garde, vous pouvez la retrouver dans la partie";

        return back()->with('childcare_refused', $message);
    }

    public function refuse($id)
    {
        Auth::user()->refuseChildcare($id);

        $message = "Vous avez bien supprimé la personne en charge de cette garde";

        return back()->with('childcare_refused', $message);
    }
}