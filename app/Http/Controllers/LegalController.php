<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LegalController extends Controller
{
    public function cgu()
    {
        return view('legal.cgu');
    }

    public function validation()
    {
        return view('legal.validate');
    }

    public function acceptCGU()
    {
        Auth::user()
            ->update(
                ['cgu' => 1]
            );

        return redirect('home');
    }
}
