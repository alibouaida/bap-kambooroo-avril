<?php

namespace App\Http\Controllers;

use App\Notifications\NewNetworkInvitation;
use App\User;
use foo\bar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use function Sodium\compare;

class NetworkController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('CguTrue');
        $this->middleware('profilUncomplet');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $friends = Auth::user()->friends();

        return view('network.index', compact('friends'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $slug
     *
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //
    }


    public function searchcontact()
    {
        return view('network.search');
    }

    public function invitations($slug)
    {

        $user = User::where('slug', $slug)->first();

        $requests = Auth::user()->friendRequests();

        return view('network.invitations', compact('user', 'slug', 'requests'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('friendship')
            ->where('user_id', '=', $id)
            ->orWhere('friend_id', '=', $id)
            ->delete();

        return back()->with('message', 'Ami supprimé');
    }

    //Ajouter ami

    public function getAdd($slug)

    {

        $user = User::where('slug', $slug)->first();

        if (!$user) {

            return redirect()->route('user.show', ['slug' => $user->slug])
                ->with('info', 'Ce utilisateur n\'existe pas');

        }

        //un user ne peux pas s'envoyer une demande

        if (Auth::user()->slug === $user->slug) {

            return redirect()->route('user.show', ['slug' => $user->slug]);

        }

        //si existe une demande en attente

        if (Auth::user()->hasFriendRequestPending($user) || $user->

            hasFriendRequestPending(Auth::user())) {

            return redirect()->route('user.show', ['slug' => $user->slug])
                ->with('info', 'Invitation déjà envoyée.');

        }

        //si demande à été deja envoyé

        if (Auth::user()->isFriendsWith($user)) {

            return redirect()->route('user.show', ['slug' => $user->slug])
                ->with('info', 'Vous êtes déjà amis.');

        }


        $user2 = User::where('slug', $user->slug)->first();
        $user2->notify(new NewNetworkInvitation(Auth::user()));

        Auth::user()->addFriend($user);

        return redirect()->route('network.index')
            ->with('info', 'Demande envoyée');

    }


    public function getAccept($slug)

    {

        $user = User::where('slug', $slug)->first();

        if (!$user) {

            return redirect()->route('user.show', ['slug' => $user->slug])
                ->with('info', 'Ce utilisateur n\'existe pas');

        }


        if (!Auth::user()->hasFriendRequestReceived($user)) {

            return redirect()->route('network.index');

        }


        Auth::user()->acceptFriendRequest($user);

        return redirect()->route('network.index')
            ->with('success', 'Demande Acceptée');

    }

    public function search(Request $request)
    {
        $query = $request->input('q');
        $results = User::where('name', 'LIKE', '%' . $query . '%')->where('id','!=', Auth::user()->id)->get();
        if (count($results) > 0)
            return view('network.search', compact('results', 'query'));
        else return view('network.search')->withMessage('Aucun résultat valide !');
    }
}
