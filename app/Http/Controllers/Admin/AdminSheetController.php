<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Sheet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class AdminSheetController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('isAdmin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sheets = Sheet::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.sheet.index')->with('sheets', $sheets);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sheet.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sheet = new Sheet();

        if ($request->hasFile('image')) {
            $imageName = time() . '.' . $request->image->getClientOriginalName();
            $request->image->move(public_path('images/sheets'), $imageName);
            $sheet->image = $imageName;
        } else {
            $sheet->image = 'default.png';
        }

        if ($request->hasFile('file_name')) {
            $fileName = time() . '.' . $request->file_name->getClientOriginalName();
            $request->file_name->move(public_path('pdf/sheets'), $fileName);
            $sheet->file_name = $fileName;
        } else {
            $sheet->file = 'default.pdf';
        }

        $sheet->title = $request->input('title');
        $sheet->age_range = $request->input('age_range');
        $sheet->content = $request->input('content');
        $sheet->user_id = Auth::user()->id;
        $sheet->save();


        return Redirect::back()->with('message', 'Fiche Créée!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sheet = Sheet::find($id);
        return view('admin.sheet.edit')->with('sheet', $sheet);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'age_range' => 'required'

        ]);

        // Édition de l'article
        $sheet = Sheet::find($id);
        // Vérification de l'auteur
        if ($sheet->user_id !== auth()->user()->id) {
            return redirect('')->with('error', 'Page non autorisée');
        }
        $sheet->title = $request->input('title');
        $sheet->content = $request->input('content');
        $sheet->age_range = $request->input('age_range');

        if ($request->hasFile('image')) {
            $imageName = time() . '.' . $request->image->getClientOriginalName();
            $request->image->move(public_path('images/sheets'), $imageName);
            $sheet->image = $imageName;
        }
        $sheet->save();

        return Redirect::back()->with('message', 'Fiche éditée!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sheet = Sheet::find($id);

        if ($sheet->user_id !== auth()->user()->id) {
            return Redirect::back();
        }
        $sheet->delete();

        return Redirect::back()->with('message', 'Article supprimé!');
    }
}
