<?php

namespace App\Http\Controllers\Admin;

use App\Childcare;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminChildCareController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('isAdmin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $childcares = Childcare::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.childcare.index')->with('childcares', $childcares);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $id = Auth::user()->id;

        $childcares = User::find($id)->childcares;


        return view('childcare.create', compact('childcares'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $childcare = new Childcare;
        $childcare->user_id = $request->user()->id;
        $childcare->children = $request->get('children');
        $childcare->begining = $request->get('begining');
        $childcare->end = $request->get('end');
        $childcare->day = $request->get('day');
        $childcare->location = $request->get('location');
        $childcare->age_range = $request->get('age_range');
        $childcare->description = $request->get('description');

        $childcare->save();

        return back()->with('message', 'Votre annonce a bien été mise en ligne');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $childcare = DB::table('create_childcare')->find($id);

        return view('admin.childcare.edit', compact('id', 'childcare'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $childcare = Childcare::find($id);

        $childcare->children = $request->get('children');
        $childcare->begining = $request->get('begining');
        $childcare->end = $request->get('end');
        $childcare->day = $request->get('day');
        $childcare->location = $request->get('location');
        $childcare->age_range = $request->get('age_range');
        $childcare->description = $request->get('description');

        $childcare->save();

        $myneeds = Auth::user()->childcares;

        return view('childcare.myneeds', compact('myneeds'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        DB::table('create_childcare')
            ->delete($id);

        return back();
    }
    public function myNeeds()
    {
        $myneeds = Auth::user()->childcares;

        return view('childcare.myneeds', compact('myneeds'));
    }

    public function myChildcares()
    {
        $mychildcares = Childcare::where('accepted_by', Auth::user()->id)->get();

        return view('childcare.mychildcares', compact('mychildcares'));
    }

    /* Permet a un user de se proposer pour une garde */
    public function acceptChildcare($id)
    {
        Auth::user()->acceptChildcare($id);

        return back();
    }

    /* Valide le user qui se propose pour la garde */
    public function confirmChildcare($id)
    {
        Auth::user()->confirmChildcare($id);

        return back();
    }

    /* Refuse le user qui se propose pour la garde */
    public function refuseChildcare($id)
    {
        Auth::user()->refuseChildcare($id);

        return back();
    }
}
