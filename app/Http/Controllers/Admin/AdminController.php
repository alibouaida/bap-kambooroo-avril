<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Childcare;
use App\Sheet;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('isAdmin');
    }

    /**
     * Display administration home
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sheets = Sheet::count();
        $articles = Article::count();
        $users = User::count();
        $childcares = Childcare::count();

        return view('admin.index', compact('sheets', 'articles', 'users', 'childcares'));
    }
}
