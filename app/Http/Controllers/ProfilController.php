<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateProfil;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class ProfilController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('CguTrue');
        $this->middleware('profilUncomplet');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $slug
     *
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $user = User::where('slug', $slug)->first();

        if (Auth::user()->slug !== $user->slug) {
            return redirect('/login');

        } else {
            return view('profil.edit', compact('user', 'slug'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $slug
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $user = User::where('slug', $slug)->first();
        if (substr($user->image_name, 0, 4) === "http") {
            $imageLink = $user->image_name;
        } else {
            $imageLink = asset('images/avatars/'.$user->image_name.'');
        }

        if (Auth::user()->slug !== $user->slug) {
            return redirect('/login');

        } else {
            return view('profil.edit', compact('user', 'slug', 'imageLink'));
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProfil $request
     * @param $slug
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfil $request, $slug)
    {
        $passwordIsOk = password_verify($request->get('password'), Auth::user()->getAuthPassword());

        if ($passwordIsOk == true) {
            $user = User::where('slug', $slug)->first();
            // $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->phone = $request->get('phone');
            $user->city = $request->get('city');
            $user->postal = $request->get('postal');


            if ($request->hasFile('image')) {
                $imageName = time() . '.' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/avatars'), $imageName);
                $user->image_name = $imageName;
            }

            $user->update();

            return redirect('profil.edit', ['slug', Auth::user()->slug]);

        } elseif (Auth::user()->password == []) {
            $user = User::where('slug', $slug)->first();
            //$user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->phone = $request->get('phone');
            $user->city = $request->get('city');
            $user->postal = $request->get('postal');

            if ($request->hasFile('image')) {
                $imageName = time() . '.' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/avatars'), $imageName);
                $user->image_name = $imageName;
            }

            $user->update();

            return Redirect::back();

        } elseif

        ($passwordIsOk == false) {
            return Redirect::back()->withErrors(['Mauvais mot de passe']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
