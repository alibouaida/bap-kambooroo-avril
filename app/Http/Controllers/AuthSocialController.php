<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Facades\Socialite;
use function Sodium\compare;

class AuthSocialController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider)
    {
        try {
            $providerUser = Socialite::driver($provider)->user();
        } catch (\Exception $e) {
            throw $e;
        }

        $user = $this->checkIfProviderIdExists($provider, $providerUser->id);

        if ($user) {
            Auth::guard()->login($user, true);
            return redirect('/home');
        }

        if ($providerUser->email !== null) {

            $user = User::where('email', $providerUser->email)->first();
            if ($user) {
                $field = $provider . '_id';
                $user->$field = $providerUser->id;
                $user->save();
                Auth::guard()->login($user, true);
                return redirect('/home');
            }
        }

        $verif = false;
        $nb = 0;
        $names = explode(" ", $providerUser->name);
        $slug = strtolower($names[0] . '.' . $names[1]);

        if (User::where('slug', $slug)->first()) {
            $alreadySlugExist = true;
        } else {
            $alreadySlugExist = false;
            $slug = strtolower($names[0] . '.' . $names[1]);
            $verif = true;
        }

        while ($alreadySlugExist != false) {
            $nb = $nb + 1;
            $slug = $slug . $nb;
            $alreadySlugExist = User::where('slug', $slug)->first();
            if (!$alreadySlugExist) {
                $verif = true;
            } else {
                $slug = strtolower($names[0] . '.' . $names[1]);
            }
        }

        if ($verif) {
            $user = User::create([
                'name'            => $providerUser->name,
                'email'           => $providerUser->email,
                'image_name'      => $providerUser->avatar,
                'firstname'       => $names[0],
                'lastname'        => $names[1],
                $provider . '_id' => $providerUser->id,
                'slug'            => $slug,
                'cgu'             => 0,
            ]);


        }


        if ($user) Auth::guard()->login($user, true);
        return redirect('/home');
    }

    public function checkIfProviderIdExists($provider, $providerId)
    {

        $field = $provider . "_id";

        $user = User::where($field, $providerId)->first();

        return $user;
    }

}
