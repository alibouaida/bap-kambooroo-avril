<?php

namespace App\Http\Controllers\Auth;

use App\Mail\VerifyMail;
use App\User;
use App\Http\Controllers\Controller;
use App\VerifyUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'phone' => 'numeric|digits:10',
            'postal' => 'numeric|digits:5',
            'image_name' => 'image',
            'cgu' => 'accepted',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     *
     * @return \App\User
     */
    protected function create(array $data)
    {

        if (empty($data['firstname']) AND empty($data['lastname']) AND !empty($data['name'])) {
            $names = explode(" ", $data['name']);
            $data['firstname'] = $names[0];
            $data['lastname'] = $names[1];
        }
        $verif = false;
        $nb = 0;
        $imageName = 'default.png';
        $slug = strtolower($data['firstname'] . '.' . $data['lastname']);

        if (User::where('slug', $slug)->first()) {
            $alreadySlugExist = true;
        } else {
            $alreadySlugExist = false;
            $slug = strtolower($data['firstname'] . '.' . $data['lastname']);
            $verif = true;
        }


        while ($alreadySlugExist != false) {
            $nb = $nb + 1;
            $slug = $slug . $nb;
            $alreadySlugExist = User::where('slug', $slug)->first();
            if (!$alreadySlugExist) {
                $verif = true;
            } else {
                $slug = strtolower($data['firstname'] . '.' . $data['lastname']);
            }
        }


        if ($verif) {
            $user = User::create([
                'name' => $data['firstname'] . ' ' . $data['lastname'] . '',
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'phone' => $data['phone'],
                'city' => $data['city'],
                'postal' => $data['postal'],
                'image_name' => $imageName,
                'slug' => $slug,
                'cgu' => 1,

            ]);

            $verifyUser = VerifyUser::create([
                'user_id' => $user->id,
                'token' => str_random(40)
            ]);

            Mail::to($user->email)->send(new VerifyMail($user));

            return $user;

        }

    }


    protected function registered(Request $request, $user)
    {
        $this->guard()->logout();
        return redirect('/login')->with('status_mail', 'Un email de confirmation vous à été envoyé.');
    }

    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();

        if(isset($verifyUser) ){
            $user = $verifyUser->user;
            if(!$user->verified) {
                $verifyUser->user->verified = 1;
                $verifyUser->user->update();
                $status = "Votre email est vérifié vous pouvez maintenant vous connecter";
            }else{
                $status = "votre email est déjà vérifié vous pouvez vous connecter.";
            }
        }else{
            return redirect('/login')->with('warning', "Désolé, votre email ne peut pas être identifié.");
        }

        return redirect('/login')->with('status', $status);
    }

}

