<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateProfil extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $user = Auth::user()->id;

        return [
            'email' => 'unique:users,email,'.$user,
            'phone' => 'numeric|digits:10|unique:users,phone,'.$user,
            'postal' => 'numeric|digits:5',
            'image'  => 'image|mimes:jpeg,png,jpg,gif|max:1000',
        ];
    }
}
