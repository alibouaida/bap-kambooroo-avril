<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyCreateChildcaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('create_childcare', function (Blueprint $table) {
            $table->dropColumn('user_name');
            $table->dropColumn('accepted_by_user_slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('create_childcare', function($table) {
            $table->string('user_name');
            $table->string('accepted_by_user_slug');
        });
    }
}
