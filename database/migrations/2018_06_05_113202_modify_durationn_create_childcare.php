<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyDurationnCreateChildcare extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('create_childcare', function (Blueprint $table) {
            $table->dropColumn('duration');
            $table->integer('begining');
            $table->integer('end');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('create_childcare', function($table) {
            $table->dropColumn('begining');
            $table->dropColumn('end');
        });
    }
}
