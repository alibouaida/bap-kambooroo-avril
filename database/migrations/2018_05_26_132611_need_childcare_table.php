<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NeedChildcareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_childcare', function (Blueprint $table){
            $table->increments('id');
            $table->string('user_id');
            $table->integer('children');
            $table->string('duration');
            $table->string('day');
            $table->string('location');
            $table->integer('age_range');
            $table->integer('accepted')->default(0);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_childcare');
    }
}
