<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyCreateChildcareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('create_childcare', function (Blueprint $table) {
            $table->string('children')->change();
            $table->string('age_range')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('create_childcare', function($table) {
            $table->dropColumn('children');
            $table->dropColumn('age_range');
        });
    }
}
