# Kambooroo 


## Environment

##### Laravel
Laravel Framework 5.5.x ([MIT license](https://opensource.org/licenses/MIT)).

##### PHP
PHP 7.2.x

##### MySQL
MariaDB 10.2.x

## Dependencies
#### Composer
        "doctrine/dbal": "^2.7",
        "fideloper/proxy": "~3.3",
        "fzaninotto/faker": "^1.7",
        "guzzlehttp/guzzle": "^6.3",
        "laravel/framework": "5.5.*",
        "laravel/scout": "^4.0",
        "laravel/socialite": "^3.0",
        "laravel/tinker": "~1.0"
#### NPM
        "bootstrap": "^4.1.1",
        "datatables.net": "^1.10.16",
        "font-awesome": "^4.7.0",
        "startbootstrap-sb-admin": "^4.0.0"
        
## Members
#### Management
* Guillaume Douceron (Development & Design)
* Laetitia Sarrazin (Marketing & Content)

#### Development
* Ali Bouaida (Back-end)
* Charles Damasse (Back-end)
* Olivier Chemla (Front-end)

#### Design
* Valentin Ferragati
* Kévin Da Costa

#### Marketing & Content
* Thomas De Oliveria
* Etienne Du Portal